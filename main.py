"""The is a simple python Flask app"""
from flask import Flask
from flask_unleash import Unleash

APP = Flask(__name__)
APP.config["UNLEASH_URL"] = "https://gitlab.com/api/v4/feature_flags/unleash/14523573"
APP.config["UNLEASH_INSTANCE_ID"] = "imFk7WZnMDK2ZHkZNT5-"
APP.config["UNLEASH_APP_NAME"] = "production" # This is the gitlab environment scope.
APP.config["UNLEASH_ENVIRONMENT"] = "my-python-app4" # I think this is ignored.
APP.config["UNLEASH_DISABLE_REGISTRATION"] = True
APP.config["UNLEASH_DISABLE_METRICS"] = True
unleash = Unleash(APP)

@APP.route("/")
def hello():
    """This is the main page of the web application"""
    flag_value_1 = unleash.client.is_enabled("is_blue")
    color = "Red"
    if flag_value_1:
        color = "Blue"
    return "Hello from Python! The Color of the Day is " + color

if __name__ == "__main__":
    APP.run(host='0.0.0.0', port=3000)
